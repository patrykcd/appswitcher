﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace AppSwitcher
{
    class AppSwitcher
    {
        [DllImport("user32")]
        private static extern bool SetWindowText(IntPtr hWnd, string lpString);

        private static List<WndTask> m_TasksList;
        static void Main()
        {
            var name = Process.GetCurrentProcess().ProcessName;
            SetProcName(name);

            var tasks = WndTask.GetRunningTasks();
            m_TasksList = new List<WndTask>(tasks);

            int i = 0;
            foreach (var task in m_TasksList)
            {
                Console.WriteLine("Index: {0}\nId:\t\t{1}\nTitle:\t\t{2}\nMain Title:\t{3}\nProcess Name:\t{4}\n{5}",
                   i,
                   task.GetId(),
                   task.GetTitle(),
                   !String.IsNullOrEmpty(task.GetMainTitle()) ? task.GetMainTitle() : "=============",
                   !String.IsNullOrEmpty(task.GetProcName()) ? task.GetProcName() : "=============",
                   "---------------------------------------");
                 i++;
            }

            Console.ReadLine();
        }

        private static void SetProcName(string name)
        {
            var proc = Process.GetCurrentProcess();
            SetWindowText(proc.MainWindowHandle, name);
        }
    }
}
