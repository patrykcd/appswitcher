﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;

namespace AppSwitcher
{
    class WndTask
    {
        private const int k_maxTitleLen = 1024;

        private IntPtr m_ptr;

        [DllImport("user32")]
        private static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

        [DllImport("user32")]
        private static extern bool IsWindowVisible(IntPtr hWnd);

        [DllImport("user32")]
        private static extern int GetWindowThreadProcessId(IntPtr hWnd, out int lpdwProcessId);

        [DllImport("user32")]
        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        public WndTask(IntPtr ptr)
        {
            m_ptr = ptr;
        }

        public string GetTitle()
        {
            var title = new StringBuilder(k_maxTitleLen);
            GetWindowText(m_ptr, title, k_maxTitleLen);
            return title.ToString();
        }

        public string GetMainTitle()
        {
            var id = new WndTask(m_ptr).GetId();
            return Process.GetProcessById(id).MainWindowTitle;
        }

        public string GetProcName()
        {
            var id = new WndTask(m_ptr).GetId();
            return Process.GetProcessById(id).ProcessName;
        }

        public bool IsVisible()
        {
            return IsWindowVisible(m_ptr);
        }

        public int GetId()
        {
            GetWindowThreadProcessId(m_ptr, out int lpdwProcessId);
            return lpdwProcessId;
        }

        private static List<WndTask> m_tasksList;
        private delegate bool WndEnumProc(IntPtr hWnd, IntPtr lParam);
        [DllImport("user32")]
        private static extern bool EnumWindows(WndEnumProc lpEnumFunc, IntPtr lParam);

        public static WndTask[] GetRunningTasks()
        {
            m_tasksList = new List<WndTask>();
            EnumWindows(EnumWindowsProcFilter, IntPtr.Zero);
            return m_tasksList.ToArray();
        }

        private static bool EnumWindowsProcFilter(IntPtr hWnd, IntPtr lParam)
        {
            WndTask task = new WndTask(hWnd);
            if (!String.IsNullOrEmpty(task.GetTitle()) & task.IsVisible())
            {
                var id = task.GetId();
                var proc = Process.GetProcessById(id);
                if (task.GetTitle() != "Program Manager" &&
                    proc.ProcessName != "ApplicationFrameHost" &&
                    proc.ProcessName != "ShellExperienceHost")
                {
                    m_tasksList.Add(task);
                } 
            }
            return true;
        }

        public enum CmdShow
        {
            Hide,
            Shownormal,
            Show_Minimized,
            Show_Maximized,
            Show_No_Activate,
            Show,
            Minimize,
            Show_Minimized_No_Active,
            Show_No_Active,
            Restore,
            Show_Default,
            Force_Minimize
        };

        public void ShowWindow(CmdShow cmd)
        {
           ShowWindow(m_ptr, (int)cmd);
        }
    }
}

